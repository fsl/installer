#!/usr/bin/env python

from __future__ import print_function

import json
import sys
import os
import os.path as op
import tempfile
import shutil
import shlex
import contextlib
import multiprocessing as mp

# py3
try:
    import http.server as http
    from io import StringIO
    from unittest import mock

# py2
except ImportError:
    from StringIO import StringIO
    import SimpleHTTPServer as http
    http.HTTPServer = http.BaseHTTPServer.HTTPServer
    http.SimpleHTTPRequestHandler.protocol_version = 'HTTP/1.0'
    import mock

import pytest
import tests.data_for_tests as data_for_tests
import fslinstaller


@contextlib.contextmanager
def mock_input(*responses):
    """Mock the built-in ``input`` or ``raw_input`` function so that it
    returns the specified sequence of ``responses``.

    Each response is returned from the ``input`` function in order, unless it
    is a callable, in which case it is called, and then the next non-callable
    response returned. This gives us a hacky way to manipulate things while
    stuck in an input REPL loop.
    """

    resp = iter(responses)

    def _input(*a, **kwa):
        n = next(resp)
        while callable(n):
            n()
            n = next(resp)
        return n

    if sys.version[0] == '2': target = '__builtin__.raw_input'
    else:                     target = 'builtins.input'

    with mock.patch(target, _input):
        yield


@contextlib.contextmanager
def indir(dir):
    """Context manager which temporarily changes into dir."""
    prevdir = os.getcwd()
    os.chdir(dir)
    try:
        yield
    finally:
        os.chdir(prevdir)


@contextlib.contextmanager
def tempdir():
    tmpdir  = tempfile.mkdtemp()
    prevdir = os.getcwd()

    try:
        os.chdir(tmpdir)
        yield tmpdir
    finally:
        os.chdir(prevdir)
        shutil.rmtree(tmpdir)


class HTTPServer(mp.Process):
    """Simple HTTP server which serves files from a specified directory.

    Intended to be used via the :func:`server` context manager function.
    """
    def __init__(self, rootdir):
        mp.Process.__init__(self)
        self.daemon = True
        self.rootdir = rootdir
        handler = http.SimpleHTTPRequestHandler
        self.server = http.HTTPServer(('', 0), handler)
        self.shutdown = mp.Event()

    def stop(self):
        self.shutdown.set()

    @property
    def port(self):
        return self.server.server_address[1]

    def run(self):
        with indir(self.rootdir):
            while not self.shutdown.is_set():
                self.server.handle_request()
            self.server.shutdown()


@contextlib.contextmanager
def server(rootdir=None):
    """Start a :class:`HTTPServer` on a separate thread to serve files from
    ``rootdir`` (defaults to the current working directory), then shut it down
    afterwards.
    """
    if rootdir is None:
        rootdir = os.getcwd()
    srv = HTTPServer(rootdir)
    srv.start()
    srv.url = 'http://localhost:{}/'.format(srv.port)
    try:
        yield srv
    finally:
        srv.stop()


@pytest.mark.parametrize(
  "v_possible, expected",
  [
    # possible         #expected
    ('1.0.0:45a459f0', '1.0.0'),
    ('1.0.0.0:45a459f0', '1.0.0'),
    ('1.0.0.1:45a459f0', '1.0.0.1'),
    ('1.0.0', '1.0.0'),
    ('1.0.0.1', '1.0.0.1'), # should keep hotfix
    ('1.0.0.0', '1.0.0') # should drop hotfix
  ]
)
def test_version(v_possible, expected):
  '''
  test that we can parse FSL versions strings correctly
  '''

  v = fslinstaller.Version(v_possible)
  assert(str(v) == expected)


@pytest.mark.parametrize(
  "command, as_root",
  [
    #command           #as root
    ('''echo "test non root"''', False),
    ('''echo "test as root"''', True)
  ]
)
def test_run_cmd_displayoutput(command, as_root):
  '''
  test that we can run as root and non root users
  '''
  fslinstaller.run_cmd_displayoutput(command, as_root=as_root)


@pytest.mark.parametrize(
  "as_root",
  [
    (False),
    (True)
  ]
)
def test_asl_gui_604_patch(as_root):
  '''
  test that out FSL 6.0.4 patch works
  '''
  patched = False
  with tempdir() as td:
      fsldir = op.join(td, 'fsl')
      print(fsldir)
      fslinstaller.install_archive(data_for_tests.asl_604_archive, fsldir)
      fslinstaller.asl_gui_604_patch(fsldir, as_root)
      with open(op.join(fsldir, 'python', 'oxford_asl', 'gui', 'preview_fsleyes.py')) as f:
          if 'ready=ready, raiseErrors=True' in f.read():
              patched = True

      assert patched


@pytest.mark.parametrize(
  "as_root",
  [
    (False),
    (True)
  ]
)
def test_asl_gui_gt_604(as_root):
  '''
  test that no patch needed for FSL > 6.0.4
  '''
  patched = False
  with tempdir() as td:
      fsldir = op.join(td, 'fsl')
      fslinstaller.install_archive(data_for_tests.asl_605_archive, fsldir)
      fslinstaller.asl_gui_604_patch(fsldir, as_root)
      with open(os.path.join(fsldir, 'python', 'oxford_asl', 'gui', 'preview_fsleyes.py')) as f:
        if 'ready=ready, raiseErrors=True' in f.read():
          patched = True

      assert patched


def test_listversions():
    fslinstaller.main(['-l'])


def test_listbuilds():
    fslinstaller.main(['-b'])


def test_typical_installation():

    datadir = op.join(op.dirname(__file__), 'test_data')
    archive = 'fsl-6.0.5-centos7_64-test_typical_installation.tar.gz'

    with open(op.join(datadir, 'manifest-test_typical_installation.json'), 'rt') as f:
        manifest = json.loads(f.read())

    with tempdir() as td, server(datadir) as srv:
        fsldir = op.join(td, 'fsl')

        with mock.patch('fslinstaller.Settings.main_server', srv.url), \
             mock.patch('fslinstaller.Settings.mirrors', [srv.url]), \
             mock.patch('fslinstaller.Settings.main_mirror', srv.url ), \
             mock.patch('fslinstaller.Settings.mirror', srv.url ), \
             mock.patch('fslinstaller.get_web_manifest', return_value=manifest), \
             mock.patch('fslinstaller.Host.o_s', 'linux'), \
             mock.patch('fslinstaller.Host.vendor', 'debian'), \
             mock.patch('fslinstaller.Host.version', fslinstaller.Version('11')):

            fslinstaller.main(shlex.split('-d {} -p'.format(fsldir)))


def test_relative_path_for_destdir():

    datadir = op.join(op.dirname(__file__), 'test_data')
    archive = 'fsl-6.0.5-centos7_64-test_typical_installation.tar.gz'

    with open(op.join(datadir, 'manifest-test_typical_installation.json'), 'rt') as f:
        manifest = json.loads(f.read())

    with tempdir() as td, server(datadir) as srv:
        archive_url = '{}/{}'.format(srv.url, archive)
        fsldir = op.join(td, 'fsl')

        def dummy_get_profile(shell):
            return op.join(td, 'dummy_profile')

        with mock.patch('fslinstaller.Settings.main_server', srv.url), \
             mock.patch('fslinstaller.Settings.mirrors', [srv.url]), \
             mock.patch('fslinstaller.Settings.main_mirror', srv.url ), \
             mock.patch('fslinstaller.Settings.mirror', srv.url ), \
             mock.patch('fslinstaller.get_web_manifest', return_value=manifest), \
             mock.patch('fslinstaller.Host.o_s', 'linux'), \
             mock.patch('fslinstaller.Host.vendor', 'debian'), \
             mock.patch('fslinstaller.Host.version', fslinstaller.Version('11')), \
             mock.patch('os.getuid', return_value=9999), \
             mock.patch('fslinstaller.get_profile', dummy_get_profile):

            fslinstaller.main(shlex.split('-d ./fsl'))

        with open('dummy_profile', 'rt') as f:
            profile = f.readlines()

        # Check that FSLDIR variable is set correctly in profile
        for line in profile:
            line = line.strip()
            if line.startswith('FSLDIR='):
                assert line == 'FSLDIR={}'.format(fsldir)
                break
            elif line.startswith('setenv FSLDIR'):
                assert line == 'setenv FSLDIR {}'.format(fsldir)
                break
        else:
            assert False


def test_relative_path_for_destdir_interactive():

    datadir = op.join(op.dirname(__file__), 'test_data')
    archive = 'fsl-6.0.5-centos7_64-test_typical_installation.tar.gz'

    with open(op.join(datadir, 'manifest-test_typical_installation.json'), 'rt') as f:
        manifest = json.loads(f.read())

    with tempdir() as td, server(datadir) as srv:
        archive_url = '{}/{}'.format(srv.url, archive)
        fsldir = op.join(td, 'fsl')

        def dummy_get_profile(shell):
            return op.join(td, 'dummy_profile')

        with mock.patch('fslinstaller.Settings.main_server', srv.url), \
             mock.patch('fslinstaller.Settings.mirrors', [srv.url]), \
             mock.patch('fslinstaller.Settings.main_mirror', srv.url ), \
             mock.patch('fslinstaller.Settings.mirror', srv.url ), \
             mock.patch('fslinstaller.get_web_manifest', return_value=manifest), \
             mock.patch('fslinstaller.Host.o_s', 'linux'), \
             mock.patch('fslinstaller.Host.vendor', 'debian'), \
             mock.patch('fslinstaller.Host.version', fslinstaller.Version('11')), \
             mock.patch('os.getuid', return_value=9999), \
             mock.patch('fslinstaller.get_profile', dummy_get_profile):

            with mock_input('./fsl'):
                fslinstaller.main([])

        with open('dummy_profile', 'rt') as f:
            profile = f.readlines()

        # Check that FSLDIR variable is set correctly in profile
        for line in profile:
            line = line.strip()
            if line.startswith('FSLDIR='):
                assert line == 'FSLDIR={}'.format(fsldir)
                break
            elif line.startswith('setenv FSLDIR'):
                assert line == 'setenv FSLDIR {}'.format(fsldir)
                break
        else:
            assert False
