#!/usr/bin/python
import datetime
import os
import unittest
import subprocess
from mock import patch, mock_open
import StringIO
import tempfile
# patch('fslinstaller.memoize', lambda x: x).start()
import fslinstaller


class TestVersion(unittest.TestCase):
    def test_version(self):
        v1 = fslinstaller.Version('0.0.0')
        self.assertEqual(
            str(v1), '0.0.0'
        )
        v1a = fslinstaller.Version('0.0.0.1')
        self.assertEqual(
            str(v1a), '0.0.0.1'
        )
        v1b = fslinstaller.Version('0.0.0.0')
        self.assertEqual(
            str(v1b), '0.0.0'
        )
        v2 = fslinstaller.Version('1.0.0')
        v2a = fslinstaller.Version('1.0.0')
        v3 = fslinstaller.Version('1.0.1')
        v4 = fslinstaller.Version('1.1.0')
        v5 = fslinstaller.Version('2.0.0')
        v6 = fslinstaller.Version('2.0.0.1')
        v7 = fslinstaller.Version('2.0.0.2')
        v8 = fslinstaller.Version('2.0.1')
        v9 = fslinstaller.Version('3.0.0')
        self.assertTrue(
            v2 == v2a
        )
        self.assertFalse(
            v2 != v2a
        )
        self.assertTrue(
            v1 < v2
        )
        self.assertTrue(
            v2 > v1
        )
        self.assertTrue(
            v1 != v2
        )
        self.assertTrue(
            v2 < v3
        )
        self.assertTrue(
            v3 > v2
        )
        self.assertTrue(
            v3 >= v2
        )
        self.assertFalse(
            v3 <= v2
        )
        self.assertFalse(
            v3 < v2
        )
        self.assertTrue(
            v3 < v4
        )
        self.assertTrue(
            v4 < v5
        )
        self.assertTrue(
            v5 < v6
        )
        self.assertTrue(
            v6 < v7
        )
        self.assertTrue(
            v7 < v8
        )
        self.assertTrue(
            v8 < v9
        )
        v = fslinstaller.Version('1.0.0:45a459f0')
        self.assertEqual(
            '1.0.0', str(v)
        )
        v = fslinstaller.Version('1.0.0.1:45a459f0')
        self.assertEqual(
            '1.0.0.1', str(v)
        )


class TestSafeDelete(unittest.TestCase):
    def test_delete(self):
        with patch.object(
                fslinstaller, 'run_cmd', autospec=True) as mock_run_cmd:
            for naughty in [
                    '/', '/usr', '/usr/bin', '/usr/local', '/bin',
                    '/sbin', '/opt', '/Library', '/System', '/System/Library',
                    '/var', '/tmp', '/var/tmp', '/lib', '/lib64', '/Users',
                    '/home', '/Applications', '/private', '/etc', '/dev',
                    '/Network', '/net', '/proc']:
                with self.assertRaises(fslinstaller.DeletionRefused):
                    fslinstaller.safe_delete(naughty)
            testfile = '/tmp/fsltest{0}'.format(datetime.datetime.now())
            open(testfile, 'a').close()
            fslinstaller.safe_delete(testfile)
            mock_run_cmd.assert_called_once_with(
                'rm -f {0}'.format(testfile), False)
            os.unlink(testfile)
            mock_run_cmd.reset_mock()
            os.mkdir(testfile)
            fslinstaller.safe_delete(testfile)
            mock_run_cmd.assert_called_once_with(
                'rm -rf {0}'.format(testfile), False)
            os.rmdir(testfile)


class TestFileContains(unittest.TestCase):
    def test_file_contains(self):
        with tempfile.NamedTemporaryFile() as tf:
            tf.write('abc')
            tf.flush()

            self.assertTrue(
                fslinstaller.file_contains(
                    tf.name, 'ab'))
            self.assertFalse(
                fslinstaller.file_contains(
                    tf.name, 'cd'
                )
            )

    def test_file_contains_1stline(self):
        with tempfile.NamedTemporaryFile() as tf:
            tf.write("abc\n")
            tf.write("def\n")
            tf.write("defg\n")
            tf.flush()

            self.assertEqual(
                'abc\n',
                fslinstaller.file_contains_1stline(
                    tf.name, 'ab'))
            self.assertEqual(
                'def\n',
                fslinstaller.file_contains_1stline(
                    tf.name, 'def'
                )
            )


class TestArchiveType(unittest.TestCase):
    def test_archive_type(self):
        self.assertEqual(
            ('tar', '-z'),
            fslinstaller.archive_type('file.tar.gz'))
        self.assertEqual(
            ('tar', '-z'),
            fslinstaller.archive_type('file.tgz'))
        self.assertEqual(
            ('tar', '-j'),
            fslinstaller.archive_type('file.tar.bz2'))
        self.assertEqual(
            ('unzip', ''),
            fslinstaller.archive_type('file.zip'))


class TestHashes(unittest.TestCase):
    def test_sha256File(self):
        content = '1234567890abcdefghijklmnopqrstuvwxyz\n'
        sha256 = (
            '10e4160443c978243e8934c21a1a5f0a'
            '1f6d69a2be9dde85b8f419c967eff9e2')
        with patch(
                    "__builtin__.open",
                    mock_open(read_data=content)) as mock_file:
            self.assertEqual(
                fslinstaller.sha256File('/tmp/filepath'),
                sha256)
            mock_file.assert_called_once_with('/tmp/filepath', 'rb')
            mock_file.return_value.close.asset_called_once()

    def test_md5File(self):
        content = '1234567890abcdefghijklmnopqrstuvwxyz\n'
        md5 = 'f6a6fe33e0a49de86681fc8c4703b7fa'
        with patch(
                    "__builtin__.open",
                    mock_open(read_data=content)) as mock_file:
            self.assertEqual(
                fslinstaller.md5File('/tmp/filepath'),
                md5
            )
            mock_file.assert_called_once_with(
                '/tmp/filepath', 'rb')


class TestUrlBuilding(unittest.TestCase):

    def test_build_url_with_protocol(self):
        self.assertEqual(
            fslinstaller.build_url_with_protocol(
                'http',
                'www.domain.com',
                ['mybase',
                 'index.html', ]
            ),
            'http://www.domain.com/mybase/index.html'
        )
        self.assertEqual(
            fslinstaller.build_url_with_protocol(
                'http',
                'www.domain.com/',
                ['mybase',
                 'index.html', ]
            ),
            'http://www.domain.com/mybase/index.html'
        )
        self.assertEqual(
            fslinstaller.build_url_with_protocol(
                'http',
                'www.domain.com/',
                ['/mybase',
                 'index.html', ]
            ),
            'http://www.domain.com/mybase/index.html'
        )
        self.assertEqual(
            fslinstaller.build_url_with_protocol(
                'http',
                'www.domain.com',
                ['mybase',
                 '/index.html', ]
            ),
            'http://www.domain.com/mybase/index.html'
        )
        self.assertEqual(
            fslinstaller.build_url_with_protocol(
                'http',
                'www.domain.com/',
                ['mybase/',
                 '/index.html', ]
            ),
            'http://www.domain.com/mybase/index.html'
        )

    def test_build_url(self):
        self.assertEqual(
            fslinstaller.build_url(
                ['mybase',
                 'index.html', ]
            ),
            'mybase/index.html'
        )
        self.assertEqual(
            fslinstaller.build_url(
                ['mybase/',
                 'index.html', ]
            ),
            'mybase/index.html'
        )
        self.assertEqual(
            fslinstaller.build_url(
                ['/mybase',
                 'index.html', ]
            ),
            'mybase/index.html'
        )
        self.assertEqual(
            fslinstaller.build_url(
                ['mybase',
                 '/index.html', ]
            ),
            'mybase/index.html'
        )
        self.assertEqual(
            fslinstaller.build_url(
                ['mybase/',
                 '/index.html', ]
            ),
            'mybase/index.html'
        )


class MockObject(object):
    def __init__(self, *args, **kwargs):
        for k, v in kwargs.items():
            self.k = v


class ManifestTests(unittest.TestCase):
    manifest_dict = {
        "linux": {
            "fedora": {
                "alias": {
                    "12": {
                        "supported": False,
                        "version": 12,
                        "parent": "centos"
                    },
                }
            },
            "centos": {
                "x86_64": {
                    "5": {
                        "5.0.9": {
                            "version": "5.0.9",
                            "instructions":
                                "https://fsl.fmrib.ox.ac.uk/"
                                "fsl/fslwiki/FslInstallation/Linux",
                            "checksum":
                                "a639db87c19bbf05232dc96a9e3154dd8b"
                                "dcab67692da796fb04ef38f5f2ccab",
                            "date": "2017-04-10",
                            "checksum_type": "sha256",
                            "notes": "",
                            "supported": True,
                            "filename": "fsl-5.0.9-centos5_64.tar.gz"
                        }
                    }
                },
            },
            "ubuntu": {
                "x86_64": {
                    "12": {
                        "5.0.9": {
                            "redirect":
                                "http://neuro.debian.net/"
                                "install_pkg.html?p=fsl-complete"
                        },
                        "5.0.10": {
                            "version": "5.0.10",
                            "instructions":
                                "https://fsl.fmrib.ox.ac.uk/fsl/"
                                "fslwiki/FslInstallation/Linux",
                            "checksum":
                                "b62cd94e2cfe53e30c8e0c6880345c010"
                                "5b1a737df9dbd23272bcc1dfb55ffbb",
                            "date": "2017-04-25",
                            "checksum_type": "sha256",
                            "notes": "",
                            "supported": True,
                            "filename": "fsl-5.0.10-centos6_64.tar.gz"
                        },
                    },
                    "13": {
                        "5.0.10": {
                            "version": "5.0.10",
                            "instructions":
                                "https://fsl.fmrib.ox.ac.uk/fsl/"
                                "fslwiki/FslInstallation/Linux",
                            "checksum":
                                "b62cd94e2cfe53e30c8e0c6880345c010"
                                "5b1a737df9dbd23272bcc1dfb55ffbb",
                            "date": "2017-04-25",
                            "checksum_type": "sha256",
                            "notes": "",
                            "supported": True,
                            "filename": "fsl-5.0.10-centos6_64.tar.gz"
                        },
                    }
                },
                "alias": {
                    "19": {
                        "supported": True,
                        "version": 13,
                        "parent": "ubuntu"
                    }
                }
            },
        },
        "installer": {
            "version": "3.0.11",
            "instructions": "",
            "checksum":
                "6828112d06266b9c172e13f98a3f"
                "d06678d5712e6647408719eea175f565c9e9",
            "date": "2018-01-03",
            "checksum_type": "sha256",
            "notes": "",
            "supported": True,
            "filename": "fslinstaller.py"
        },
        "feeds": {
            "5.0.9": {
                "version": "5.0.9",
                "instructions": "",
                "checksum":
                    "9a146b25784c617b44738f82ecfe30481"
                    "1ca5c1479edefa9b88c759f00893eb5",
                "date": "2017-04-10",
                "checksum_type": "sha256",
                "notes": "",
                "supported": True,
                "filename": "fsl-5.0.9-feeds.tar.gz"
            },
        },
        "darwin": {
            "apple": {
                "x86_64": {
                    "10": {
                        "5.0.9": {
                            "version": "5.0.9",
                            "instructions":
                                "https://fsl.fmrib.ox.ac.uk/fsl/"
                                "fslwiki/FslInstallation/MacOsX",
                            "checksum":
                                "83854513fe51b0eb6348ceab95e1502e"
                                "24d17ec1bae2d4a390f90d7346628432",
                            "date": "2017-04-10",
                            "checksum_type": "sha256",
                            "notes": "",
                            "supported": True,
                            "filename": "fsl-5.0.9-macosx_64.tar.gz"
                        }
                    },
                }
            }
        }
    }

    @patch(
        'fslinstaller.get_web_manifest', auto_spec=True,
        return_value=manifest_dict)
    @patch('fslinstaller.Host', auto_spec=True)
    def test_get_releases_real(self, mock_host, mock_manifest):
        mock_host.o_s = 'linux'
        mock_host.arch = 'x86_64'
        mock_host.os_type = 'posix'
        mock_host.supported = True
        mock_host.vendor = 'ubuntu'
        mock_host.version = fslinstaller.Version('12.04')
        mock_host.bits = 64

        self.assertDictEqual(
            fslinstaller.get_releases("anyoldurl"),
            {
                "5.0.9": {
                    "redirect":
                        "http://neuro.debian.net/"
                        "install_pkg.html?p=fsl-complete"
                },
                "5.0.10": {
                    "version": "5.0.10",
                    "instructions":
                        "https://fsl.fmrib.ox.ac.uk/fsl/"
                        "fslwiki/FslInstallation/Linux",
                    "checksum":
                        "b62cd94e2cfe53e30c8e0c6880345c010"
                        "5b1a737df9dbd23272bcc1dfb55ffbb",
                    "date": "2017-04-25",
                    "checksum_type": "sha256",
                    "notes": "",
                    "supported": True,
                    "filename": "fsl-5.0.10-centos6_64.tar.gz"
                },
            }
        )

    @patch(
        'fslinstaller.get_web_manifest', auto_spec=True,
        return_value=manifest_dict)
    @patch('fslinstaller.Host', auto_spec=True)
    @patch('sys.stdout', new_callable=StringIO)
    def test_get_releases_newer(self, mock_stdout, mock_host, mock_manifest):
        mock_host.o_s = 'linux'
        mock_host.arch = 'x86_64'
        mock_host.os_type = 'posix'
        mock_host.supported = True
        mock_host.vendor = 'ubuntu'
        mock_host.version = fslinstaller.Version('14.04')
        mock_host.bits = 64

        self.assertDictEqual(
            fslinstaller.get_releases("anyoldurl"),
            {
                "5.0.10": {
                    "version": "5.0.10",
                    "instructions":
                        "https://fsl.fmrib.ox.ac.uk/fsl/"
                        "fslwiki/FslInstallation/Linux",
                    "checksum":
                        "b62cd94e2cfe53e30c8e0c6880345c010"
                        "5b1a737df9dbd23272bcc1dfb55ffbb",
                    "date": "2017-04-25",
                    "checksum_type": "sha256",
                    "notes": "",
                    "supported": True,
                    "filename": "fsl-5.0.10-centos6_64.tar.gz"
                },
            }
        )
        self.assertEqual(
            mock_stdout.getvalue(),
            '\x1b[34m\x1b[1m[Warning]\x1b[0m ubuntu 14 not officially '
            'supported - trying to '
            'locate support for an earlier version - this may not work\n'
        )

    @patch(
        'fslinstaller.get_web_manifest', auto_spec=True,
        return_value=manifest_dict)
    @patch('fslinstaller.Host', auto_spec=True)
    @patch('sys.stdout', new_callable=StringIO)
    def test_get_releases_alias(self, mock_stdout, mock_host, mock_manifest):

        mock_host.o_s = 'linux'
        mock_host.arch = 'x86_64'
        mock_host.os_type = 'posix'
        mock_host.supported = True
        mock_host.vendor = 'ubuntu'
        mock_host.version = fslinstaller.Version('19.04')
        mock_host.bits = 64

        self.assertDictEqual(
            fslinstaller.get_releases("anyoldurl"),
            {
               "5.0.10": {
                    "version": "5.0.10",
                    "instructions":
                    "https://fsl.fmrib.ox.ac.uk/fsl/"
                    "fslwiki/FslInstallation/Linux",
                    "checksum":
                    "b62cd94e2cfe53e30c8e0c6880345c010"
                    "5b1a737df9dbd23272bcc1dfb55ffbb",
                    "date": "2017-04-25",
                    "checksum_type": "sha256",
                    "notes": "",
                    "supported": True,
                    "filename": "fsl-5.0.10-centos6_64.tar.gz"
                },
            }
        )
        self.assertEqual(
            mock_stdout.getvalue(),
            '\x1b[34m\x1b[1m[Warning]\x1b[0m ubuntu 19 not officially'
            ' supported - trying to locate support for an earlier '
            'version - this may not work\n'
        )


class DoInstallTests(unittest.TestCase):
    @patch('fslinstaller.MsgUser', auto_spec=True)
    @patch('fslinstaller.Host', auto_spec=True)
    def setUp(self, mock_host, mock_msg):
        self.mock_host = mock_host
        self.mock_msg = mock_msg

    def setupLinux(self, vendor, version, libc, bits='64'):
        self.mock_host.o_s = 'linux'
        self.mock_host.arch = 'x86_64'
        self.mock_host.applever = ''
        self.mock_host.os_type = 'posix'
        self.mock_host.supported = True
        self.mock_host.vendor = vendor
        self.mock_host.version = version
        self.mock_host.glibc = libc
        self.mock_host.bits = bits

    def test_unsupported(self):
        self.setupLinux('arch', '3', '2.2.5', '32')
        options = MockObject()
        options.test_installer = False
        options.supported = False
        options.quiet = False
        with self.assertRaises(
                fslinstaller.InstallError,
                fslinstaller.do_install, options, fslinstaller.Settings()
                ) as Ex:
            self.assertEquals(
                str(Ex),
                "Unsupported host - you could try buiding form source")


if __name__ == '__main__':
    unittest.main()
