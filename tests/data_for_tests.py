import os
base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

asl_604_archive = os.path.join(base_dir, 'tests', 'test_data', 'fsl-6.0.4-centos7_64.tar.gz')
asl_605_archive = os.path.join(base_dir, 'tests', 'test_data', 'fsl-6.0.5-centos7_64.tar.gz')