Version 3.3.0
=============

Changes made:

* The legacy `manifest.json`, describing FSL releases 6.0.5.2 and older, is now
  embedded in the `fslinstaller.py` script.
* No longer bother downloading `fslmirrorlist.txt` - only support downloading
  from https://fsl.fmrib.ox.ac.uk/


Version 3.2.3
=============

Changes made:

* Allow the destination directory to be specified as a relative path.


Version 3.2.2
=============

Changes made:

* A couple of python 2/3-related fixes, and a fix to the handling of default
  answers to questions.


Version 3.2.1
=============

Changes made:

* Fallback to assuming CentOS 7 if the Linux distribution cannot be identified.


Version 3.2.0
=============

Changes made:

* The installer is now compatible with all versions of Python 2.7 and newer.


Version 3.1.0
=============

Changes made:

* New `-b` option to list all available FSL builds (e.g. `centos7_64`,
  `macOS_64`).
* New `-B` option to download a specific build, ignoring the host environment.
* If running on an Apple M1 architecture, the x86 build is downloaded.
* Output of the `--listversions` option is ordered sensibly.


Version 3.0.21
==============

Changes made:

* Minor internal re-organisation

Version 3.0.20
==============

New features:

* Added patcher for faulty 6.0.4 release

Version 3.0.19
==============

Bugs fixed:

* Linux OS aliases and alias parent version numbers now mapped better

Version 3.0.18
==============
New features:

* Added support for zsh (macOS 10.15)

Bugs fixed:

* Incorrect multiple setting of FSLDIR within profile (no practical effect)

Version 3.0.17
==============
Bugs fixed:

* incorrect version parsing for >= FSL6.0.2

Version 3.0.16
==============
Bugs fixed:

* Debug output when downloading a file fails was unable to print the error message

Version 3.0.15
==============
Bugs fixed:

* MATLAB setup file content being interpreted by python string formatter

Version 3.0.14
==============
Bugs fixed:

* MATLAB environment not being configured correctly

Version 3.0.13
==============
Bugs fixed:

* -e option fixed
* -E option corrected to create profile in expected location
* Correction of some error reports
* Improvements to installation code

Version 3.0.12
==============
General code clean up.

Bugs fixed:

* Improved handling of OS 'aliases'

Version 3.0.11
==============
Bugs fixed:

* Correctly display available versions where some releases are available through third-parties
* Don't ask for FSLDIR location when listing available versions

New features:

* Cleaned up terminal output during normal usage

Version 3.0.10
=============
Bugs fixed:

* Fail gracefully when installer run on an OS that is supported by some other means (e.g. a package manager)

Version 3.0.9
=============
Bugs fixed:

* Ensure installer could be overwritten in the future
* Refactor post installation code to minimise repeated code blocks
* Fix typo in an exception handler

Version 3.0.8.1
=============
Bugs fixed:

* Revert being unable to delete an old version of FSL not crashing the installer as the next task will crash, hiding the real error.

Version 3.0.8
=============
Bugs fixed:

* Fix silly bug in debug message reporting

Version 3.0.7
=============
Bugs fixed:

* Being unable to delete an old version of FSL does not result in an installer crash

Version 3.0.6
=============
Bugs fixed:

* Correct behaviour when running as the non-root user, e.g. when updating user profiles
* Clear the spinner when errors occur

Version 3.0.5
=============
Bugs fixed:

* When installer self updates don't try to install into the FSL directory until after the install

Version 3.0.4
=============
Bugs fixed:

* Prevent crash when OS is not supported

Version 3.0.3
=============
Bugs fixed:

* Correct handling of re-installation question

Version 3.0.2
=============
New features:

* Improved handling of inappropriate install folder selection
* Asks whether to re-install when installed and new version are the same

Version 3.0.1
=============
Bugs fixed:

* Fixed error with mirror site handling

Version 3.0.0
=============
Re-written installer:

* Able to request specific versions of FSL
* Download source code and FEEDS with the installer
* Adds version specific post install processing
* Major clean up of source code

Version 2.0.22
==============
New features:

* Centos 7 changed to supported platform (continues to use the Centos 6 binaries)
* Warning on Centos 7 to install libpng12 and libmng for FSLView support

Version 2.0.21
==============
New features:

* OS X 10.11 changed to supported platform
* macOS 10.12 added as an unsupported platform

Version 2.0.20
==============
New features:

* Installation allowed on OS X 10.11

Version 2.0.19
==============
Bugs fixed:

* Corrected error handler when unable to save new installer version to the existing FSL install location.

Version 2.0.18
==============
Bugs fixed:

* Corrected error handler when connection to the FSL website times out.

Version 2.0.17
==============
New features:

* Reworked unsupported OS handling
* Installation allowed on OS X 10.10

Version 2.0.16
==============
Bugs fixed:

* Corrected check for Centos/RHEL 7 to prevent inadvertant warnings on Centos 6.

Version 2.0.15
==============
New features:

* Will allow installation on Centos/RHEL 7 using the Centos 6 binaries. This isn't full support, for example, additional software may need to be installed (eg libmng, libpng12)

Version 2.0.14
==============
Bugs fixed:

* Error reporting on older python releases fixed

Version 2.0.13
==============
New features:

* Will allow installation on Fedora using the Centos 6 binaries. This isn't full support, for example, additional software may need to be installed (eg libpng-compat


Version 2.0.12
==============
New features:

* Support OS X 10.9 (Mavericks)

Version 2.0.11
==============
Bugs fixed:

* Work arounds for NFS funnies when handling temporary files

New features:

* Download auto-resumes on failure (for example VM guests suffering from a network hangup on large file downloads)

Version 2.0.10
==============
Bugs fixed:

* Typo in debug messages
* Extra debug output
* Fixed handling of pre-existing symlinks in /Applications folder
* Fixed behaviour when a corrupt old FSL install is found in the install location

Version 2.0.9
=============
Bugs fixed:

* Error handling when shell name is not recognised

Version 2.0.8
=============
New features:

* Improved error handling when downloading files
* Extra debug messages in fastest mirror code

Version 2.0.7
=============
New features:

* Added support for 'hotfix' version number
* Added hidden option to allow testing of installer

Version 2.0.6
=============
Bugs fixed:

* Fixed error reporting code
* Added code to allow debugging of rare networking issue

New features:

* Validates the FSLDIR environment variable to allow installer to repair miss-configured environment

Version 2.0.5
=============
Bugs fixed:

* Indent errors

Version 2.0.4
=============
Bugs fixed:

* Correctly identifies Fedora and refuses to install
* Doesn't crash when checking for new FSL releases

Version 2.0.3
=============
Bugs fixed:

* If a .bash_profile/.cshrc file needs to be created it doesn't add new lines after each entry

Version 2.0.2
=============
UNRELEASED
Bugs fixed:

* Installer incorrectly allows relative paths to be specified - now checks for and refuses to install to these paths

Version 2.0.1
=============
Bugs fixed:

* Failed to delete temporary folder created in install location (looks like fsl-XXXXXX.XXXXXX)
* Fails to setup MATLAB environment on OS X
* Fails to create /Applications links for FSLView on OS X
* Ingores install into folders other than /usr/local (the default)
* Crash if install folder doesn't exist and needs to be created by a superuser
* Crash when trying to report that it cannot contact FSL website
* NamedTemporaryFile lacks a 'delete' option on Python 2.4
* Urllib2.urlopen lacks a 'timeout' option on Python 2.4
* OS detection broken on Python 2.4
* Various Red Hat Linux variants were not recognised correcly on Python 2.6
* Crashes if .bash_profile doesn't exist
* Python bug when install on an OS X machine where the user's home folder is on an NFS share
* Fails to install new version into the FSLDIR when self update completes and FSLDIR owned by superuser

Version 2.0.0
=============

* New release written in Python
* Downloads correct FSL version for your OS, verifies and installs, setting up your environment
* Supports updating of installer should bug fixes be released
