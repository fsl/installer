#!/usr/bin/env bash
pip install pytest coverage pytest-cov mock
export PYTHONPATH=$(pwd)
export SHELL=/bin/bash
pytest -v -s --cov=fslinstaller tests
