# FSLINSTALLER


The `fslinstaller` is a python script that handles downloading and installing
an FSL release. This is the **legacy** installer script, used to install
versions of FSL 6.0.5.2 and older. For newer FSL releases, the new
`fslinstaller` script can be found at https://git.fmrib.ox.ac.uk/fsl/conda/installer

## Usage

```
Usage: fslinstaller.py [options]

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -d DESTDIR, --dest=DESTDIR
                        Install into folder given by DESTDIR - e.g.
                        /usr/local/fsl
  -e                    Only setup/update your environment
  -E                    Setup/update the environment for ALL users
  -v                    Print version number and exit
  -c, --checkupdate     Check for FSL updates - needs an internet connection

  Advanced Install Options:
    These are advanced install options

    -l, --listversions  List available versions of FSL
    -V FSLVERSION, --fslversion=FSLVERSION
                        Download the specific version FSLVERSION of FSL
    -s, --source        Download source code for FSL
    -F, --feeds         Download FEEDS
    -q, --quiet         Silence all messages - useful if scripting install
    -p                  Don't setup the environment

  Debugging Options:
    These are for use if you have a problem running this installer.

    -f ARCHIVEFILE, --file=ARCHIVEFILE
                        Install a pre-downloaded copy of the FSL archive
    -C CHECKSUM, --checksum=CHECKSUM
                        Supply the expected checksum for the pre-downloaded
                        FSL archive
    -T CHECKSUM_TYPE, --checksum-type=CHECKSUM_TYPE
                        Specify the type of checksum
    -M, --nochecksum    Don't check the pre-downloaded FSL archive
    -D                  Switch on debug messages
```

# Testing

### Create a python 2 conda environment

**note**
python2.7 installed via conda on ubuntu systems will give different version info via `platform.linux_distribution()`. The returned string is not compatible with the version class `Version()` in `fslinstaller.py`.

`conda create -n python2 python=2.7 pytest pytest-cov coverage`

### or use your system python 2.X
`pip install pytest pytest-cov coverage`

### Run tests from project root

`python -m pytest -s --cov-report term --cov=fslinstaller tests`

# Deployment
- run tests first and double check a real install!
- upload the new master version of `fslinstaller.py` to the FSL release server
  - https://fsl.fmrib.ox.ac.uk/fsldownloads_registration/admin/
